<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Services;

class APIVendorRating extends Model
{
    private function connectAPI()
    {
        $client = Services::curlrequest();

        $url = "https://portal2.incoe.astra.co.id/vendor_rating/api/Approve_json";
        $response = $client->request('GET', $url);

        return $response;
    }

    public function getItem()
    {
        $response = $this->connectAPI();

        $data['supplier']['results'] = json_decode($response->getBody(), true);

        return $data;
    }
}
