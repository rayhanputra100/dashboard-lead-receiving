<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Services;

class APIReportDN extends Model
{

    public function getItem($tanggal)
    {
        $client = Services::curlrequest();

        $url = "https://portal2.incoe.astra.co.id/e-wip/api/report_dn/" . $tanggal;

        $response = $client->request('GET', $url);

        $data = json_decode($response->getBody(), true);

        // Mengurutkan data berdasarkan 'tanggal' secara ascending
        usort($data, function ($a, $b) {
            return strcmp($a['tanggal'], $b['tanggal']);
        });

        return $data;
    }
}
