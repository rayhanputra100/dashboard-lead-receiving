<?php

namespace App\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Models\TArea;
use App\Models\APIVendorRating;
use App\Models\APIReportDN;

class Data extends BaseController
{
    protected $areaModel;
    protected $apiModelVR;
    protected $apiModelRDN;
    public function __construct()
    {
        $this->areaModel = new TArea();
        $this->apiModelVR = new APIVendorRating();
        $this->apiModelRDN = new APIReportDN();
    }

    private function suppriler()
    {
        $formattedDatas = []; //array penampung data yang sudah diformat
        $uniquePlatNo = []; //array penampung nama supplier yang sudah unik
        //melakukan iterasi terhadap hasil fetch data dari supplier

        $filteredResults = [];
        $drivers = [];

        foreach ($this->apiModelVR->getItem() as $datTruck) {
            foreach ($datTruck['results']['results'] as $truck) {
                $platNo = $truck['kendaraan_polisi'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if (!in_array($platNo, $uniquePlatNo)) {
                    $uniquePlatNo[] = $platNo; //tambahkan ke array uniqueSupplierNames
                } else {
                    continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                }
            }
        }

        foreach ($this->apiModelVR->getItem() as $datSupplier) {
            foreach ($datSupplier['results']['results'] as $supplier) {

                // $url = 'https://portal2.incoe.astra.co.id/vendor_rating/api/dn_json/' . $datSupplier['no_dn'];
                // $response = file_get_contents($url);
                // $data = json_decode($response, true);
                $supplierName = $supplier['nama_supplier'];
                $limTime = "16-MAR-2023 00:30";
                $fLimTime = date_create($limTime);
                $timePlanIn = date_create($supplier['date_delivery_plan']);
                $fTimePlanIn = date("H", strtotime($supplier['date_delivery_plan'])) * 60 + date("i", strtotime($supplier['date_delivery_plan']));
                $fTimeSecIn = date("H", strtotime($supplier['receipt_security'])) * 60 + date("i", strtotime($supplier['receipt_security']));
                $fTimeSecOut = date("H", strtotime($supplier['date_done'])) * 60 + date("i", strtotime($supplier['date_done']));
                $fTimeQC = date("H", strtotime($supplier['qa_date'])) * 60 + date("i", strtotime($supplier['qa_date']));
                $fTimeWH = date("H", strtotime($supplier['warehouse_date'])) * 60 + date("i", strtotime($supplier['warehouse_date']));

                $timeMin = $timePlanIn->getTimestamp() - $fLimTime->getTimestamp();
                $timeMax = $timePlanIn->getTimestamp() + $fLimTime->getTimestamp();
                // $total_minutes = date("H", strtotime($timeDiff)) * 60 + date("i", strtotime($timeDiff));
                $timeMaxFormatted = date_create('@' . $timeMax)->format('h:i');
                $timeMinFormatted = date_create('@' . $timeMin)->format('h:i');
                $dateNow = date_format(date_create($supplier['date_delivery_plan']), 'd-M-Y');
                $past = date('d-m-Y', strtotime('-1 day'));
                $formattedData = []; //array penampung data supplier yang sudah diformat

                //melakukan iterasi terhadap hasil fetch data dari TArea
                foreach ($this->areaModel->getItem() as $areaItem) {
                    if ($supplierName === $areaItem['name_supplier']) {
                        $formattedData = [
                            'tPlanIn' => date_format(date_create($supplier['date_delivery_plan']), 'H:i'),
                            'tSecIn' => date_format(date_create($supplier['receipt_security']), 'H:i'),
                            'tQC' => date_format(date_create($supplier['qa_date']), 'H:i'),
                            'tWHC' => date_format(date_create($supplier['warehouse_date']), 'H:i'),
                            'tSecOut' => date_format(date_create($supplier['date_done']), 'H:i'),
                            'dPlanIn' => date_format(date_create($supplier['date_delivery_plan']), 'd-m-Y'),
                            'dSecIn' => date_format(date_create($supplier['receipt_security']), 'd-m-Y'),
                            'dQC' => date_format(date_create($supplier['qa_date']), 'd-m-Y'),
                            'dWHC' => date_format(date_create($supplier['warehouse_date']), 'd-m-Y'),
                            'dSecOut' => date_format(date_create($supplier['date_done']), 'd-m-Y'),
                            'nama_supplier' => $supplierName,
                            'noDN' => $supplier['no_dn'],
                            'priority' => $supplier['URGENT'],
                            'linkItem' => 'https://portal2.incoe.astra.co.id/vendor_rating/api/dn_json/' . $supplier['no_dn'],
                            'area' => $areaItem['area'],
                            'platNo' => $supplier['kendaraan_polisi'],
                            'timeDiff' => $fTimeSecOut - $fTimeSecIn,
                            'timeDiffSecIn' => $fTimeSecIn - $fTimePlanIn,
                            'timeDiffQC' => ((date_format(date_create($supplier['receipt_security']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($supplier['receipt_security']), 'd-m-Y') == $past) ? 0 : $fTimeQC - $fTimeSecIn),
                            'timeDiffWHC' => ((date_format(date_create($supplier['qa_date']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($supplier['qa_date']), 'd-m-Y') == $past) ? 0 : $fTimeWH - $fTimeQC),
                            'timeDiffSecOut' => ((date_format(date_create($supplier['warehouse_date']), 'H:i') == '07:00') || (date_format(date_create($supplier['warehouse_date']), 'd-m-Y') == $past) ? 0 : $fTimeSecOut - $fTimeWH),
                            'nDriver' => $supplier['driver'],
                            'max' => $timeMaxFormatted,
                            'min' => $timeMinFormatted

                            // 'iQTY' => $fixData['QTY'],
                            // 'iPONO' => $fixData['PONO'],
                            // 'iITEM' => $fixData['ITEM'],
                            // 'iPO' => $fixData['PO'],
                            // 'count' => $count,
                        ];
                        break;
                    }
                }
                //jika formattedData tidak kosong, maka masukkan data tersebut ke array formattedDatas
                if (!empty($formattedData)) {
                    $formattedData['onTime'] = $formattedData['tQC'] > $formattedData['tSecIn']
                        && $formattedData['tQC'] < $formattedData['tWHC']
                        && $formattedData['tQC'] < $formattedData['tSecOut']
                        && $formattedData['tWHC'] > $formattedData['tSecIn']
                        && $formattedData['tWHC'] > $formattedData['tQC']
                        && $formattedData['tWHC'] < $formattedData['tSecOut']
                        && $formattedData['tSecOut'] > $formattedData['tQC']
                        && $formattedData['tSecOut'] > $formattedData['tWHC']
                        && $formattedData['tSecOut'] > $formattedData['tQC'];

                    $formattedDatas[] = $formattedData;
                }
            }
        }


        foreach ($formattedDatas as $truck) {
            $driver = $truck['platNo'];
            if (!in_array($driver, $drivers)) {
                $filteredResults[] = $truck;
                $drivers[] = $driver;
            }
            $count = count($drivers); //untuk menghitung total truck
        }

        $allData = [
            'formattedDatas' => $formattedDatas,
            'formattedDatasTruck' => $filteredResults,
            'count' => $count,
            'dateNow' => $dateNow,
            'past' => $past,
            'uniquePlat' => $uniquePlatNo
        ];

        return $allData;
    }

    private function selisih()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueTanggal = [];
        $uniqueDate = [];
        $selisihIMPORT = [];
        $selisihKNFU = [];
        $selisihKIML = [];
        $totalSelisihHarian = [];

        foreach ($f as $item) {
            $date = $item['tanggal'];
            $formattedDate = date('d', strtotime($date));
            if (!in_array($formattedDate, $uniqueDate)) {
                $uniqueDate[] = $formattedDate;
            }
        }

        foreach ($f as $item) {
            $date = $item['tanggal'];
            if (!in_array($date, $uniqueTanggal)) {
                $uniqueTanggal[] = $date;
            }
        }

        foreach ($uniqueTanggal as $tanggal) {
            $totalSelisihIMPORT = 0;
            $totalSelisihKNFU = 0;
            $totalSelisihKIML = 0;

            foreach ($f as $item) {
                if ($item['tanggal'] == $tanggal) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalSelisihIMPORT += $item["selisih"];
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalSelisihKNFU += $item["selisih"];
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalSelisihKIML += $item["selisih"];
                    }
                }
            }

            $selisihKIML[$tanggal] = $totalSelisihKIML;
            $selisihKNFU[$tanggal] = $totalSelisihKNFU;
        }

        $jsonData = [
            [
                "y" => $uniqueDate,
                "a" => count($selisihKNFU) + count($selisihKIML),
                "b" => array_values($selisihKIML),
                "c" => array_values($selisihKNFU),
            ]
        ];

        return $jsonData;
    }
    private function selisihBySup()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueSupplier = [];
        $selisihIMPORT = [];
        $selisihKNFU = [];
        $selisihKIML = [];

        foreach ($f as $item) {
            $supplier = $item['supplier'];
            if (!in_array($supplier, $uniqueSupplier)) {
                $uniqueSupplier[] = $supplier;
            }
        }

        foreach ($uniqueSupplier as $supplier) {
            $totalSelisihIMPORT = 0;
            $totalSelisihKNFU = 0;
            $totalSelisihKIML = 0;

            foreach ($f as $item) {
                if ($item['supplier'] == $supplier) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalSelisihIMPORT += $item["selisih"];
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalSelisihKNFU += $item["selisih"];
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalSelisihKIML += $item["selisih"];
                    }
                }
            }

            $selisihIMPORT[$supplier] = $totalSelisihIMPORT;
            $selisihKNFU[$supplier] = $totalSelisihKNFU;
            $selisihKIML[$supplier] = $totalSelisihKIML;
        }

        $jsonData = [
            [
                "y" => $uniqueSupplier,
                "a" => array_values($selisihIMPORT),
                "b" => array_values($selisihKNFU),
                "c" => array_values($selisihKIML),
            ]
        ];

        return $jsonData;
    }
    private function reportDN()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueTanggal = [];
        $uniqueDate = [];
        $BeratIMPORT = [];
        $BeratKNFU = [];
        $BeratKIML = [];
        $AktualIMPORT = [];
        $AktualKNFU = [];
        $AktualKIML = [];
        $AktualTotal = [];

        foreach ($f as $item) {
            $date = $item['tanggal'];
            $formattedDate = date('d', strtotime($date));
            if (!in_array($formattedDate, $uniqueDate)) {
                $uniqueDate[] = $formattedDate;
            }
        }

        foreach ($f as $item) {
            $date = $item['tanggal'];
            if (!in_array($date, $uniqueTanggal)) {
                $uniqueTanggal[] = $date;
            }
        }


        foreach ($uniqueTanggal as $tanggal) {
            $totalBeratIMPORT = 0;
            $totalBeratKNFU = 0;
            $totalBeratKIML = 0;
            $totalBeratAktualIMPORT = 0;
            $totalBeratAktualKNFU = 0;
            $totalBeratAktualKIML = 0;

            foreach ($f as $item) {
                if ($item['tanggal'] == $tanggal) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalBeratIMPORT += intval($item["berat_supplier"]);
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalBeratKNFU += intval($item["berat_supplier"]);
                        $totalBeratAktualKNFU += floatval($item["berat_aktual"]);
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalBeratKIML += intval($item["berat_supplier"]);
                        $totalBeratAktualKIML += floatval($item["berat_aktual"]);
                    }
                }
            }


            $BeratIMPORT[$tanggal] = $totalBeratIMPORT;
            $BeratKNFU[$tanggal] = $totalBeratKNFU;
            $BeratKIML[$tanggal] = $totalBeratKIML;
            $AktualIMPORT[$tanggal] = $totalBeratAktualIMPORT;
            $AktualKNFU[$tanggal] = $totalBeratAktualKNFU;
            $AktualKIML[$tanggal] = $totalBeratAktualKIML;
            $AktualTotal[$tanggal] = $totalBeratAktualKIML + $totalBeratAktualKNFU;
        }

        $jsonData = [
            [
                "y" => $uniqueDate,
                "a" => array_values($BeratIMPORT),
                "b" => array_values($BeratKNFU),
                "c" => array_values($BeratKIML),
                "d" => array_values($AktualIMPORT),
                "e" => array_values($AktualKNFU),
                "f" => array_values($AktualKIML),
                "g" => array_values($AktualTotal),
            ]
        ];

        return $jsonData;
    }

    public function index()
    {
        $reportDN = $this->reportDN();
        $selisih = $this->selisih();
        $selisihBySup = $this->selisihBySup();
        $bulan_tahun = [];
        $tanggal = $this->request->getGet('tanggal');
        for ($bulan = 1; $bulan <= 12; $bulan++) {
            $tahun = date('Y'); // Get the current year
            $bulan_formatted = sprintf("%02d", $bulan); // Convert the month number to a 2-digit format with leading zeros
            $bulan_tahun[] = $tahun . "-" . $bulan_formatted;
        }

        $data = [
            'tanggal' => $tanggal,
            'bulan_tahun' => $bulan_tahun,
            'reportDN' => $reportDN,
            'selisih' => $selisih,
            'selisihBySup' => $selisihBySup
        ];
        return view('pages/dashboardReceive', $data);
    }

    public function exportExcel()
    {
        $allDatas = $this->suppriler();
        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();

        $activeWorksheet->getColumnDimension('A')->setWidth(34, 'px');
        $activeWorksheet->getColumnDimension('B')->setWidth(196, 'px');
        $activeWorksheet->getColumnDimension('C')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('D')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('E')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('F')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('G')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('H')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('I')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('J')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('K')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('L')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('M')->setWidth(95, 'px');

        $activeWorksheet->setCellValue('A1', 'NO');
        $activeWorksheet->setCellValue('B1', 'NAME SUPPLIER');
        $activeWorksheet->setCellValue('C1', 'LICENSE PLATE');
        $activeWorksheet->setCellValue('D1', 'DRIVER');
        $activeWorksheet->setCellValue('E1', 'DELIVERY');
        $activeWorksheet->setCellValue('F1', 'AREA');
        $activeWorksheet->setCellValue('G1', 'PLANT IN');
        $activeWorksheet->setCellValue('H1', 'SEC IN');
        $activeWorksheet->setCellValue('I1', 'QC');
        $activeWorksheet->setCellValue('J1', 'WHC');
        $activeWorksheet->setCellValue('K1', 'SEC OUT');
        $activeWorksheet->setCellValue('L1', 'RANGE TIME');
        $activeWorksheet->setCellValue('M1', 'RESULT');

        $row = 2;
        $no = 1;

        foreach ($allDatas['formattedDatas'] as $datSuppliers) {
            $activeWorksheet->setCellValue('A' . $row, $no++);
            $activeWorksheet->setCellValue('B' . $row, $datSuppliers['nama_supplier']);
            $activeWorksheet->setCellValue('C' . $row, $datSuppliers['platNo']);
            $activeWorksheet->setCellValue('D' . $row, $datSuppliers['nDriver']);
            $activeWorksheet->setCellValue('E' . $row, $datSuppliers['noDN']);
            $activeWorksheet->setCellValue('F' . $row, $datSuppliers['area']);
            $activeWorksheet->setCellValue('G' . $row, $datSuppliers['tPlanIn']);
            if ($datSuppliers['dSecIn'] == '01-01-1980' || $datSuppliers['dSecIn'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('H' . $row, "-");
            } else {
                $activeWorksheet->setCellValue('H' . $row, $datSuppliers['tSecIn']);
            }
            if ($datSuppliers['dQC'] == '01-01-1980' || $datSuppliers['dQC'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('I' . $row, "-");
            } else {
                $activeWorksheet->setCellValue('I' . $row, $datSuppliers['tQC']);
            }
            if ($datSuppliers['tWHC'] == '07:00' || $datSuppliers['dWHC'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('J' . $row, "-");
            } else {
                $activeWorksheet->setCellValue('J' . $row, $datSuppliers['tWHC']);
            }
            if ($datSuppliers['dSecOut'] == '01-01-1980' || $datSuppliers['dSecOut'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('K' . $row, "-");
            } else {
                $activeWorksheet->setCellValue('K' . $row, $datSuppliers['tSecOut']);
            }
            if ($datSuppliers['dSecIn'] == '01-01-1980' && $datSuppliers['dSecOut'] == '01-01-1980' && $datSuppliers['dSecIn'] == $allDatas['past'] && $datSuppliers['dSecOut'] == $allDatas['past'] || $datSuppliers['dSecOut'] == '01-01-1980' || $datSuppliers['dSecIn'] == '01-01-1980' || $datSuppliers['dSecOut'] == $allDatas['past'] || $datSuppliers['dSecIn'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('L' . $row, "-");
            } else {
                $activeWorksheet->setCellValue('L' . $row, $datSuppliers['timeDiff']);
            }
            if ($datSuppliers['dSecIn'] == $allDatas['past'] && $datSuppliers['dSecOut'] == $allDatas['past'] && $datSuppliers['dWHC'] == $allDatas['past'] && $datSuppliers['dQC'] == $allDatas['past']) {
                $activeWorksheet->setCellValue('M' . $row, "NG");
            } else {
                $activeWorksheet->setCellValue('M' . $row, $datSuppliers['onTime'] ? 'OK' : 'NG');
            }
            $row++;
        }
        // Set the header content type and attachment filename


        $filename = 'Supplier-Incoming-Report at - ' . date('Y-m-d') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }
}
