<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>


<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="card d-flex justify-content-center" dir="ltr">
                    <div class="row">
                        <form action="<?= site_url() ?>" method="get">
                            <label for="filter" class="form-label">Filter</label>
                            <select class="form-control" id="filter" name="tanggal" placeholder="Tanggal">
                                <option value="<?php date('Y-m') ?>">--Default--</option>
                                <?php foreach ($bulan_tahun as $v) : ?>
                                    <option value="<?= $v; ?>" <?= $tanggal === $v ? 'selected' : ''; ?>><?= $v; ?></option>
                                <?php endforeach ?>
                            </select>
                            <button class="btn btn-primary" type="submit">Filter</button>
                        </form>
                        <div class="col-12 ">

                            <figure class="highcharts-figure">
                                <div id="containerReceive"></div>
                            </figure>


                        </div><!-- end col-->
                        <div class="col-6">


                            <figure class="highcharts-figure">
                                <div id="containerVarian"></div>
                            </figure>

                        </div><!-- end col-->
                        <div class="col-6">


                            <figure class="highcharts-figure">
                                <div id="containerVarianBySup"></div>
                            </figure>

                        </div><!-- end col-->
                        <div class="col-12 ">

                            <figure class="highcharts-figure">
                                <div id="containerTruck"></div>
                            </figure>


                        </div><!-- end col-->
                    </div>
                </div> <!-- end card-->

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->
<?= $this->endSection(); ?>