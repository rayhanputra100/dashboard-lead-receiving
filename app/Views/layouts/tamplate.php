<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Dashboard Warehouse</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Warehouse Management System" name="Rayhan" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Bootstrap Tables css -->
    <link href="<?php echo base_url(); ?>assets/libs/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

    <!-- My CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/iPTCBI.ico">

    <!-- Plugins css -->
    <link href="<?php echo base_url(); ?>assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/css/config/creative/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/config/creative/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <link href="<?php echo base_url(); ?>assets/css/config/creative/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/config/creative/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

    <!-- Chartist Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/chartist/chartist.min.css">

    <!-- third party css -->
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-bs5/css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-select-bs5/css//select.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
    <!-- icons -->
    <link href="<?php echo base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />

    <!-- Instascan -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/adapter.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/instascan.min.js"></script> -->


</head>

<!-- body start -->

<body class="loading" data-layout-mode="horizontal" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

    <?= $this->include('/layouts/navbar'); ?>
    <br />
    <?= $this->renderSection('content'); ?>

    <!-- Right Sidebar -->
    <!-- <div class="right-bar">
            <div data-simplebar class="h-100"> -->

    <!-- Nav tabs -->
    <!-- <ul class="nav nav-tabs nav-bordered nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link py-2 active" data-bs-toggle="tab" href="#settings-tab" role="tab">
                            <i class="mdi mdi-cog-outline d-block font-22 my-1"></i>
                        </a>
                    </li>
                </ul> -->

    <!-- Tab panes -->
    <!-- <div class="tab-content pt-0">  
                    <div class="tab-pane active" id="settings-tab" role="tabpanel">
                        <h6 class="fw-medium px-3 m-0 py-2 font-13 text-uppercase bg-light">
                            <span class="d-block py-1">Theme Settings</span>
                        </h6>

                        <div class="p-3">
                            <div class="alert alert-warning" role="alert">
                                <strong>Customize </strong> the overall color scheme, sidebar menu, etc.
                            </div>

                            <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Color Scheme</h6>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="color-scheme-mode" value="light" id="light-mode-check" checked />
                                <label class="form-check-label" for="light-mode-check">Light Mode</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="color-scheme-mode" value="dark" id="dark-mode-check" />
                                <label class="form-check-label" for="dark-mode-check">Dark Mode</label>
                            </div> -->

    <!-- Width -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Width</h6>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="width" value="fluid" id="fluid-check" checked />
                                <label class="form-check-label" for="fluid-check">Fluid</label>
                            </div>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="width" value="boxed" id="boxed-check" />
                                <label class="form-check-label" for="boxed-check">Boxed</label>
                            </div> -->

    <!-- Menu positions -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Layout Positon</h6>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="menus-position" value="fixed" id="fixed-check" checked />
                                <label class="form-check-label" for="fixed-check">Fixed</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="menus-position" value="scrollable" id="scrollable-check" />
                                <label class="form-check-label" for="scrollable-check">Scrollable</label>
                            </div> -->

    <!-- Topbar -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Topbar</h6>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="topbar-color" value="dark" id="darktopbar-check" checked />
                                <label class="form-check-label" for="darktopbar-check">Dark</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="topbar-color" value="light" id="lighttopbar-check" />
                                <label class="form-check-label" for="lighttopbar-check">Light</label>
                            </div>


                            <div class="d-grid mt-4">
                                <button class="btn btn-primary" id="resetBtn">Reset to Default</button>
                            </div>

                        </div>

                    </div>
                </div>

            </div> end slimscroll-menu -->
    <!-- </div> -->
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <!-- <div class="rightbar-overlay"></div> -->
    <!-- Vendor js -->
    <script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>

    <!-- Plugins js-->
    <script src="<?php echo base_url(); ?>assets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/apexcharts/apexcharts.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/libs/selectize/js/standalone/selectize.min.js"></script>

    <!-- third party js -->
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tableHTMLExport.js"></script>
    <!-- third party js ends -->

    <!-- Datatables init -->
    <script src="<?php echo base_url(); ?>assets/js/pages/datatables.init.js"></script>

    <!--Chartist Chart-->
    <script src="<?php echo base_url(); ?>assets/libs/chartist/chartist.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min.js"></script>
    <!-- Bootstrap Tables js -->
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-table/bootstrap-table.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/hightchart/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightchart/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightchart/modules/export-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightchart/modules/accessibility.js"></script>

    <!-- Init js -->
    <script src="<?php echo base_url(); ?>assets/js/pages/bootstrap-tables.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/chartist.init.js"></script>
    <!-- Dashboar 1 init js-->
    <script src="<?php echo base_url(); ?>assets/js/pages/dashboard-1.init.js"></script>

    <!-- App js-->
    <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
    <!-- Reset data with scan qr -->

    <!-- Reset data Force! -->
    <script>
        $(document).ready(function() {
            var t = $("#demo-custom-toolbar"),
                n = $("#demo-delete-row-paksa");
            t.on(
                    "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                    function() {
                        n.prop("disabled", !t.bootstrapTable("getSelections").length);
                    }
                ),
                n.click(function() {
                    var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                        return e[2];
                    });
                    if (e.length > 0) {
                        // Mengubah nilai dari $id_array dengan nilai array e
                        var id_array = e;
                        // Lakukan iterasi pada setiap ID dan memanggil metode update untuk masing-masing ID
                        $.each(id_array, function(index, id) {
                            $.ajax({
                                url: '/listreset/' + id,
                                type: 'GET',
                                success: function(response) {
                                    // Lakukan sesuatu jika metode update berhasil
                                    console.log('Reset data berhasil untuk ID ' + id);
                                },
                                error: function(xhr, status, error) {
                                    // Lakukan sesuatu jika metode update gagal
                                    console.log('Reset data gagal untuk ID ' + id);
                                }
                            });
                        });
                        // Arahkan pengguna ke halaman `/listdata`
                        window.location.href = '/listdata';
                    } else {
                        // Jika tidak ada baris yang dipilih, maka tampilkan alert kepada pengguna.
                        alert("Please select at least one row.");
                    }
                });
        })
    </script>
    <!-- Reset data receh with scan qr  -->

    <!-- Reset data receh Force! -->
    <script>
        $(document).ready(function() {
            var t = $("#custom-toolbar-receh"),
                n = $("#delete-row-receh-paksa");
            t.on(
                    "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                    function() {
                        n.prop("disabled", !t.bootstrapTable("getSelections").length);
                    }
                ),
                n.click(function() {
                    var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                        return e[2];
                    });
                    if (e.length > 0) {
                        // Mengubah nilai dari $id_array dengan nilai array e
                        var id_array = e;
                        // Lakukan iterasi pada setiap ID dan memanggil metode update untuk masing-masing ID
                        $.each(id_array, function(index, id) {
                            $.ajax({
                                url: '/listreset/' + id,
                                type: 'GET',
                                success: function(response) {
                                    // Lakukan sesuatu jika metode update berhasil
                                    console.log('Reset data berhasil untuk ID ' + id);
                                },
                                error: function(xhr, status, error) {
                                    // Lakukan sesuatu jika metode update gagal
                                    console.log('Reset data gagal untuk ID ' + id);
                                }
                            });
                        });
                        // Arahkan pengguna ke halaman `/listdata`
                        window.location.href = '/listdata';
                    } else {
                        // Jika tidak ada baris yang dipilih, maka tampilkan alert kepada pengguna.
                        alert("Please select at least one row.");
                    }
                });
        })
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerReceive", {
            chart: {
                type: "column",
            },
            title: {
                text: "LEAD RECEIVE",
                align: "center",
            },
            xAxis: {
                categories: [
                    <?php foreach ($reportDN as $v) { ?>
                        <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                        <?php } ?>
                    <?php } ?>
                ],
            },
            yAxis: [{
                min: <?php echo floatval(0); ?>,
                title: {
                    text: "Berat Supplier",
                },
                stackLabels: {
                    enabled: true,
                },
            }],
            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 10,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y:.2f}<br/>Total: {point.stackTotal:.2f}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            if (this.y !== 0) {
                                return this.y;
                            }
                        },
                        inside: true,
                    },
                },
                spline: {
                    marker: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "K-NFU",
                    dataLabels: {
                        enabled: true,
                    },
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b / 1000); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-IML",
                    dataLabels: {
                        enabled: true,
                    },
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c / 1000); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "Aktual K-NFU",
                    stack: 1,
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['e'] as $e) { ?>
                                <?php echo floatval($e); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                    },
                },
                {
                    name: "Aktual K-IML",
                    stack: 1,
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['f'] as $f) { ?>
                                <?php echo floatval($f); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                    },
                },
                {
                    name: "Total Aktual",
                    stack: 2,
                    type: "spline",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['g'] as $g) { ?>
                                <?php echo floatval($g); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                    },
                },
            ],
        });
    </script>






    <script type="text/javascript">
        Highcharts.chart("containerVarian", {
            chart: {
                type: "column",
            },
            title: {
                text: "TOTAL VARIANCE",
            },
            xAxis: [{
                    categories: [
                        <?php foreach ($selisih as $v) { ?>
                            <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    categories: ["Total Variance"], // Menambahkan kategori "Total Variance"
                },
            ],
            credits: {
                enabled: false,
            },
            plotOptions: {
                column: {
                    borderRadius: "25%",
                    colorByPoint: true, // Mengaktifkan pewarnaan berdasarkan poin
                    colors: ["green"], // Mengatur warna hijau
                    negativeColor: "red", // Mengatur warna merah untuk angka minus
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            var color = this.point.stack === 1 ? "blue" : (this.y >= 0 ? "green" : "red");
                            return '<span style="color:' + color + '">' + this.y + "</span>";
                        },
                    },
                },
            },
            series: [{
                    name: "K-IML Variance",
                    data: [
                        <?php foreach ($selisih as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-NFU Variance",
                    data: [
                        <?php foreach ($selisih as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                
            ],
        });
    </script>





    <script type="text/javascript">
        Highcharts.chart("containerVarianBySup", {
            chart: {
                type: "column",
            },
            title: {
                text: "VARIANCE BY SUPPLIER",
            },
            xAxis: {
                categories: ["<?= $tanggal ?>"],
            },
            credits: {
                enabled: false,
            },
            plotOptions: {
                column: {
                    borderRadius: "25%",
                    colorByPoint: true, // Mengaktifkan pewarnaan berdasarkan poin
                    colors: ["green"], // Mengatur warna hijau
                    negativeColor: "red", // Mengatur warna merah untuk angka minus
                },
            },
            series: [{
                    name: "K-NFU",
                    data: [
                        <?php foreach ($selisihBySup as $v) { ?>
                            <?php echo floatval($v['b'][1]); ?>,
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            var color = this.y >= 0 ? "green" : "red"; // Menentukan warna berdasarkan nilai positif atau negatif
                            return '<span style="color:' + color + '">' + this.y + "</span>";
                        },
                    },
                },
                {
                    name: "K-IML",
                    data: [
                        <?php foreach ($selisihBySup as $v) { ?>
                            <?php echo floatval($v['c'][2]); ?>,
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            var color = this.y >= 0 ? "green" : "red"; // Menentukan warna berdasarkan nilai positif atau negatif
                            return '<span style="color:' + color + '">' + this.y + "</span>";
                        },
                    },
                },
            ],
        });
    </script>
    <script type="text/javascript">
        Highcharts.chart("containerTruck", {
            chart: {
                type: "column",
            },
            title: {
                text: "QTY TRUCK",
                align: "center",
            },
            xAxis: {
                categories: [
                    <?php foreach ($reportDN as $v) { ?>
                        <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                        <?php } ?>
                    <?php } ?>
                ],
            },
            yAxis: {
                min: 0,
                title: {
                    text: "Count trophies",
                },
                stackLabels: {
                    enabled: true,
                },
            },
            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 70,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "K-NFU",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-IML",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
            ],
        });
    </script>



</body>

</html>